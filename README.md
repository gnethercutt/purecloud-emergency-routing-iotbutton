# Overview

This is a Lambda service that brokers the control of a PureCloud [emergency group for routing](https://help.mypurecloud.com/articles/emergencies/) via an [AWS IoT Button](https://aws.amazon.com/iotbutton/).


---
# Getting started

- [Create an emergency group](https://apps.mypurecloud.com/directory/#/admin/routing/emergencies)

- [Configure call routings to leverage an alternate call flow based on the emergency group](https://apps.mypurecloud.com/directory/#/admin/routing/ivrs)

- [Create a client credentials grant](https://apps.mypurecloud.com/directory/#/admin/integrations/oauth)

- [Configure your IoT button](https://docs.aws.amazon.com/iot/latest/developerguide/configure-iot.html)

- Deployment
  - Install purecloud sdk dependencies: ```npm install```
  - Preconditions: 
    - [install the serverless framework](https://serverless.com/framework/docs/providers/aws/guide/installation/)
    - [configure aws credentials](https://serverless.com/framework/docs/providers/aws/guide/credentials/)
  - ```
$ serverless deploy -v --client-id OAUTH_CLIENT_ID_HERE --client-secret OAUTH_CLIENT_SECRET_HERE --emergency-group-id EMERGENCY_GROUP_HERE --email EMAIL_ADDRESS_FOR_NOTIFICATIONS_HERE --serial IOT_BUTTON_SERIAL_NUMBER_HERE
```

# Actions

- Single click: activates the emergency routing group specified by the --emergency-group-id cli option
- Double click: deactivates the emergency routing group

Both actions also generate an email to the specified recipient, provided they accepted the SNS subscription that is generated upon the initial deployment.