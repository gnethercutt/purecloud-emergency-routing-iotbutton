'use strict';

const AWS = require('aws-sdk');

const SNS = new AWS.SNS({ apiVersion: '2010-03-31' });
const PURECLOUD = require('purecloud-platform-client-v2');
const client = PURECLOUD.ApiClient.instance;
const architect = new PURECLOUD.ArchitectApi();

function setEmergencyRouting(enabled) {
    client.loginClientCredentialsGrant(process.env.client_id, process.env.client_secret)
      .then(() => {
        var emergencyGroupId = process.env.emergency_group_id;
        architect.getArchitectEmergencygroup(emergencyGroupId)
          .then(function(data) {
            console.log(`getArchitectEmergencygroup success! data: ${JSON.stringify(data, null, 2)}`);
            data.enabled = enabled;
            architect.putArchitectEmergencygroup(emergencyGroupId, data)
              .then(function(result) {
                console.log(`putArchitectEmergencygroup success! data: ${JSON.stringify(result, null, 2)}`);
              })
              .catch(function(err) {
                console.log('There was a failure calling putArchitectEmergencygroup');
                console.error(err);
              });
          })
    })
    .catch((err) => {
      console.log(err);
    });    
}

/**
 * The following JSON template shows what is sent as the payload:
{
    "serialNumber": "GXXXXXXXXXXXXXXXXX",
    "batteryVoltage": "xxmV",
    "clickType": "SINGLE" | "DOUBLE" | "LONG"
}
 *
 * A "LONG" clickType is sent if the first press lasts longer than 1.5 seconds.
 * "SINGLE" and "DOUBLE" clickType payloads are sent for short clicks.
 *
 */
exports.action = (event, context, callback) => {
    console.log('Received event:', event.clickType);
    var enabledState = (event.clickType==='SINGLE') ? true : false;
    var enabledStateString = (event.clickType==='SINGLE') ? 'activated' : 'deactivated';
    var topicArn = process.env.snsTopic;

    setEmergencyRouting(enabledState);

    console.log(`Publishing to topic ${topicArn}`);
    const params = {
          Message: `${event.serialNumber} -- processed by Lambda\nBattery voltage: ${event.batteryVoltage}\nClick type: ${event.clickType}`,
          Subject: `PureCloud Emergency routing ${enabledStateString} from your IoT Button ${event.serialNumber}`,
          TopicArn: topicArn,
    };
    SNS.publish(params, callback);
};